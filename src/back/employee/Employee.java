package back.employee;

import java.net.URL;
public class Employee {
    enum EmployeesRole{ADMIN, APPROVER, WORKER}

    String firstName;
    String secondName;
    String login;
    String password;
    int id;
    String gender;
    String email;
    EmployeesRole role;

    public Employee(String firstName, String secondName, String login, String password,
                    int id, String gender, EmployeesRole role) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.login = login;
        this.password = password;
        this.role = role;
        this.id = id;
        this.gender = gender;
        this.email = firstName + secondName + "@work.com";
    }

    @Override
    public String toString() {
        return this.role +  ": "
                + this.firstName + " "
                + this.secondName + " "
                + this.login + " "
                + this.password + " "
                + this.id + " "
                + this.gender + " "
                + this.email;
    }
}
